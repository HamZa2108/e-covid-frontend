# Base image
FROM node:12-alpine

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the rest of the app files to the container
COPY . .

# Build the app for production
RUN npm run build --prod

# Use NGINX to serve the app
FROM nginx:1.21-alpine
COPY --from=0 /app/dist/e-covid-frontend /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
